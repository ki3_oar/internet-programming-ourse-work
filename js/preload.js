var preloader = document.getElementById('hellopreloader');
var btn = document.getElementsByClassName('button')[0];

//num - кількість всіх елементів
var krok = 16; //крок показу 16 блоків
var current = 16; //поточна позиція, 16 було показано, почин. з 17
var limit; //для відстеження кінця

function anime(){
    preloader.style.display='block';
    setTimeout(function (){
        btn.style.display='none';
        if (num-current>16) {
            limit = current + krok;
        }
        else {
            limit = current + (num-current);
        }
        for (let j = current; j < limit; j++) {
            elem = document.getElementsByClassName('block')[j];
            elem.style.display = 'block'; //показати блок
        }
        current += krok;

        preloader.style.display='none'; //убрати анімацію
        if (num - current > 0) {
            btn.style.display = 'block';
        }
    },2000);
}

