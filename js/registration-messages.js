var btn = document.getElementsByClassName('btn-success')[0];
var login = document.getElementById('login');
var nameUser = document.getElementById('name');
var pass = document.getElementById('pass');
btn.disabled = true;

function checkParams() {
    if (login.value.length < 5 || login.value.length > 100 || nameUser.value.length<4 || nameUser.value.length>50 || pass.value.length<6 || pass.value.length>32)  {
        btn.disabled = true;
        btn.style.cssText = 'background: gray;'
    }
    else {
        btn.disabled = false;
        btn.style.background = '#0e8393';
    }
}


