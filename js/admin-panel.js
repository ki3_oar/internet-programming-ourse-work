var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ['July', 'August', 'September', 'October', 'November', 'December'],
        datasets: [{
            label: 'кількість коментарів в місяць',
            backgroundColor: 'white',
            borderColor: 'lightblue',
            data: [c1, c2, c3, c4, c5, c6]
        }]
    },
    options: {}
});

var ctx1 = document.getElementById('myChart1').getContext('2d');
var chart1 = new Chart(ctx1, {
    type: 'line',
    data: {
        labels: ['July', 'August', 'September', 'October', 'November', 'December'],
        datasets: [{
            label: 'кількість фільмів в місяць',
            backgroundColor: 'white',
            borderColor: 'lightblue',
            data: [f1, f2, f3, f4, f5, f6]
        }]
    },
    options: {}
});

var ctx2 = document.getElementById('myChart2').getContext('2d');
var chart2 = new Chart(ctx2, {
    type: 'line',
    data: {
        labels: ['July', 'August', 'September', 'October', 'November', 'December'],
        datasets: [{
            label: 'кількість користувачів в місяць',
            backgroundColor: 'white',
            borderColor: 'lightblue',
            data: [u1, u2, u3, u4, u5, u6]
        }]
    },
    options: {}
});




