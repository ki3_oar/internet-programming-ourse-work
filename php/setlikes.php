<?php
function setLikes($induction, $id, $path)
{
    if ($_COOKIE['user'] != '') {
        $user_login = $_COOKIE['user'];
        $likes = mysqli_query($induction, "SELECT * FROM historylikesanddislikes WHERE `login`='$user_login' AND `idFilm`='$id'");
        if (mysqli_num_rows($likes) > 0) {
            $likesRes = mysqli_fetch_assoc($likes);
            $liked = $likesRes['likeFlag'];
        } else {
            $createLikes = mysqli_query($induction, "INSERT INTO `historylikesanddislikes`(`login`,`idFilm`,`likeFlag`) values ('$user_login','$id',0)");
            $liked = 0;
        }
        $getLatterFilm = mysqli_query($induction, "SELECT * FROM `laterfilms` WHERE `idFilm`='$id' AND `login`='$user_login'");
        if (mysqli_num_rows($getLatterFilm) > 0) {
            $getLatter = mysqli_fetch_assoc($getLatterFilm);
            $latter = $getLatter['laterFlag'];
        } else {
            $createLatterFilm = mysqli_query($induction, "INSERT INTO `laterfilms`(`idFilm`,`login`,`laterFlag`) values ('$id','$user_login',0)");
            $latter = 0;
        }
        $getStarFilm = mysqli_query($induction, "SELECT * FROM `starfilms` WHERE `idFilm`='$id' AND `login`='$user_login'");
        if (mysqli_num_rows($getStarFilm) > 0) {
            $getStar = mysqli_fetch_assoc($getStarFilm);
            $star = $getStar['starFlag'];
        } else {
            $createStarFilm = mysqli_query($induction, "INSERT INTO `starfilms`(`idFilm`,`login`,`starFlag`) values ('$id','$user_login',0)");
            $star = 0;
        }
    }


    if (isset($_POST['btn-like-avtorizated'])) {
        if ($liked) {
            $setLike = mysqli_query($induction, "UPDATE `historylikesanddislikes` SET `likeFlag`=0 WHERE `login`='$user_login' AND `idFilm`='$id'");
            $setCount = mysqli_query($induction, "UPDATE `films` SET `countLikes`=`countLikes`-1 WHERE `idFilm`='$id'");
            header('Location: '.$path);
        } else {
            $setLike = mysqli_query($induction, "UPDATE `historylikesanddislikes` SET `likeFlag`=1 WHERE `login`='$user_login' AND `idFilm`='$id'");
            $setCount = mysqli_query($induction, "UPDATE `films` SET `countLikes`=`countLikes`+1 WHERE `idFilm`='$id'");
            header('Location: '.$path);
        }
    }

    if (isset($_POST['btn-latter-avtorizated'])) {
        if ($latter) {
            $setLike = mysqli_query($induction, "UPDATE `laterfilms` SET `laterFlag`=0 WHERE `idFilm`='$id' AND `login`='$user_login'");
            header('Location: '.$path);
        } else {
            $setLike = mysqli_query($induction, "UPDATE `laterfilms` SET `laterFlag`=1 WHERE `idFilm`='$id' AND `login`='$user_login'");
            header('Location: '.$path);
        }
    }

    if (isset($_POST['btn-star-avtorizated'])) {
        if ($star) {
            $setLike = mysqli_query($induction, "UPDATE `starfilms` SET `starFlag`=0 WHERE `idFilm`='$id' AND `login`='$user_login'");
            header('Location: '.$path);
        } else {
            $setLike = mysqli_query($induction, "UPDATE `starfilms` SET `starFlag`=1 WHERE `idFilm`='$id' AND `login`='$user_login'");
            header('Location: '.$path);
        }
    }
    $array[0] = $liked;
    $array[1] = $latter;
    $array[2] = $star;
    return $array;
}



