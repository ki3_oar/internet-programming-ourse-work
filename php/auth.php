<?php
include 'connection.php';
use configuration\Config as config;
$mysql = new mysqli(config::$ip,config::$name,config::$pass,config::$db_name);

$login = filter_var(trim($_POST['login']),FILTER_SANITIZE_STRING);
$pass = filter_var(trim($_POST['pass']),FILTER_SANITIZE_STRING);
if (strlen($login)==0){
    setcookie('wrongLogin','login',time()+3600,'/');
} else
    setcookie('wrongLogin','login',time()-3600,'/');

if (strlen($pass)==0){
    setcookie('wrongPass','pass',time()+3600,'/');
} else
    setcookie('wrongPass','pass',time()-3600,'/');
$pass = md5($pass . "gdf78njdd83");

if ($_POST['checkAdmin']=="Yes"){
    $getAdmin = $mysql->query("SELECT * FROM `admins` WHERE `login`='$login' AND `password`='$pass'");
    $admin = mysqli_fetch_assoc($getAdmin);

    if (count($admin)==0){
        setcookie('error-admin', 'smth', time() + 3600, '/');
        header('Location: /pages/authorization.php');
    }
    else {
        setcookie('error-admin', 'smth', time() - 3600, '/');
        setcookie('admin', $admin['login'], time() + 3600, '/');
        $mysql->close();
        header('Location: ../pages/admin/admin-panel.php');
    }
}
else {
    $result = $mysql->query("SELECT * FROM `users` WHERE `login`='$login' AND `password`='$pass'");
    $user = mysqli_fetch_assoc($result);

    if (count($user) == 0) {
        setcookie('error', 'smth', time() + 3600, '/');
        header('Location: /pages/authorization.php');
    } else {
        setcookie('error', 'smth', time() - 3600, '/');
        setcookie('user', $user['login'], time() + 3600, '/');
        setcookie('user_id', $user['id'], time() + 3600, '/');
        $mysql->close();
        header('Location: /');
    }
}
?>

