<?php
function Check($parametr)
{
    if ($parametr < 1000) {
        echo $parametr;
    }
    if ($parametr >= 1000 and $parametr < 100000) {
        echo round(($parametr / 1000), 1);
        echo 'k';
    }
    if ($parametr >= 100000 and $parametr < 1000000) {
        echo intdiv($parametr, 1000);
        echo 'k';
    }
    if ($parametr >= 1000000 and $parametr < 1000000000) {
        echo round(($parametr / 1000000), 1);
        echo 'M';
    }
}