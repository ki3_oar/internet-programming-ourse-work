-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Час створення: Січ 12 2021 р., 00:41
-- Версія сервера: 10.3.22-MariaDB
-- Версія PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `site_db`
--

-- --------------------------------------------------------

--
-- Структура таблиці `admins`
--

CREATE TABLE `admins` (
  `idAdmin` int(11) NOT NULL,
  `nameAdmin` varchar(100) NOT NULL,
  `login` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `admins`
--

INSERT INTO `admins` (`idAdmin`, `nameAdmin`, `login`, `password`) VALUES
(1, 'Адміністратор', 'administrator', '9b1bf6c4dfdbbad982b1b6222d42f77b');

-- --------------------------------------------------------

--
-- Структура таблиці `comments`
--

CREATE TABLE `comments` (
  `idComment` int(11) NOT NULL,
  `loginAuthor` varchar(100) NOT NULL,
  `idFilm` int(11) NOT NULL,
  `textComment` varchar(1000) NOT NULL,
  `datePublication` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `comments`
--

INSERT INTO `comments` (`idComment`, `loginAuthor`, `idFilm`, `textComment`, `datePublication`) VALUES
(1, 'user1', 1, 'Крутий мальтик!', '2021-01-09');

-- --------------------------------------------------------

--
-- Структура таблиці `films`
--

CREATE TABLE `films` (
  `idFilm` int(11) NOT NULL,
  `nameFilm` varchar(255) NOT NULL,
  `yearFilm` int(4) NOT NULL,
  `country` varchar(100) NOT NULL,
  `genre` varchar(50) NOT NULL,
  `timeFilm` int(11) NOT NULL,
  `descriptionFilm` varchar(2000) NOT NULL,
  `page` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `video` varchar(255) NOT NULL,
  `countViews` int(11) NOT NULL DEFAULT 0,
  `countLikes` int(11) NOT NULL DEFAULT 0,
  `dateCreation` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `historylikes`
--

CREATE TABLE `historylikes` (
  `idHistory` int(11) NOT NULL,
  `login` varchar(100) NOT NULL,
  `idFilm` int(11) NOT NULL,
  `likeFlag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `historylikes`
--

INSERT INTO `historylikes` (`idHistory`, `login`, `idFilm`, `likeFlag`) VALUES
(1, 'user1', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `laterfilms`
--

CREATE TABLE `laterfilms` (
  `idLatter` int(11) NOT NULL,
  `idFilm` int(11) NOT NULL,
  `login` varchar(100) NOT NULL,
  `laterFlag` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `laterfilms`
--

INSERT INTO `laterfilms` (`idLatter`, `idFilm`, `login`, `laterFlag`) VALUES
(1, 1, 'user1', 0);

-- --------------------------------------------------------

--
-- Структура таблиці `starfilms`
--

CREATE TABLE `starfilms` (
  `idStar` int(11) NOT NULL,
  `login` varchar(100) NOT NULL,
  `idFilm` int(11) NOT NULL,
  `starFlag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `starfilms`
--

INSERT INTO `starfilms` (`idStar`, `login`, `idFilm`, `starFlag`) VALUES
(1, 'user1', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `users`
--

CREATE TABLE `users` (
  `idUser` int(11) NOT NULL,
  `nameUser` varchar(100) NOT NULL,
  `login` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL,
  `dateCreation` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `users`
--

INSERT INTO `users` (`idUser`, `nameUser`, `login`, `password`, `dateCreation`) VALUES
(1, 'Роман', 'romka777', '78151c13277b4a28bb8466059d16ec2c', '2021-01-09 15:31:24'),
(2, 'Пользователь1', 'user1', '78151c13277b4a28bb8466059d16ec2c', '2021-01-09 15:31:24');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`idAdmin`);

--
-- Індекси таблиці `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`idComment`);

--
-- Індекси таблиці `films`
--
ALTER TABLE `films`
  ADD PRIMARY KEY (`idFilm`);

--
-- Індекси таблиці `historylikes`
--
ALTER TABLE `historylikes`
  ADD PRIMARY KEY (`idHistory`);

--
-- Індекси таблиці `laterfilms`
--
ALTER TABLE `laterfilms`
  ADD PRIMARY KEY (`idLatter`);

--
-- Індекси таблиці `starfilms`
--
ALTER TABLE `starfilms`
  ADD PRIMARY KEY (`idStar`);

--
-- Індекси таблиці `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idUser`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `admins`
--
ALTER TABLE `admins`
  MODIFY `idAdmin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблиці `comments`
--
ALTER TABLE `comments`
  MODIFY `idComment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблиці `films`
--
ALTER TABLE `films`
  MODIFY `idFilm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблиці `historylikes`
--
ALTER TABLE `historylikes`
  MODIFY `idHistory` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблиці `laterfilms`
--
ALTER TABLE `laterfilms`
  MODIFY `idLatter` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблиці `starfilms`
--
ALTER TABLE `starfilms`
  MODIFY `idStar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблиці `users`
--
ALTER TABLE `users`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
