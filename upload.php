<?php
include './php/connection.php';
use configuration\Config as config;
$induction = mysqli_connect(config::$ip,config::$name,config::$pass,config::$db_name);

$nameFilm = $_POST['nameFilm'];
$yearFilm = $_POST['yearFilm'];
$country = $_POST['country'];
$genre = $_POST['genre'];
$timeFilm = $_POST['timeFilm'];
$descriptionFilm = $_POST['descriptionFilm'];
if ($_POST['countViews']!='') {
    $countViews = $_POST['countViews'];
} else {
    $countViews = 0;
}
if ($_POST['countLikes']!='') {
    $countLikes = $_POST['countLikes'];
} else {
    $countLikes = 0;
}
$image = '/images/'.$_FILES['imageFilm']['name'];
$video = '/videos/'.$_FILES['videoFilm']['name'];
$page = '/pages/'.substr($_FILES['videoFilm']['name'], 0, -4).'.php';

//загрузка в тимчасову папку temp
move_uploaded_file($_FILES['imageFilm']['tmp_name'], 'temp/'.$_FILES['imageFilm']['name']);
move_uploaded_file($_FILES['videoFilm']['tmp_name'], 'temp/'.$_FILES['videoFilm']['name']);
//картинку вставляємо в папку images, а відео в папку videos
rename('temp/'.$_FILES['imageFilm']['name'],'images/'.$_FILES['imageFilm']['name']);
rename('temp/'.$_FILES['videoFilm']['name'],'videos/'.$_FILES['videoFilm']['name']);
//копіюємо сторінку з прикладу
$namePage = substr($_FILES['videoFilm']['name'],0,-4);
copy('pages/example/example.php','pages/'.$namePage.'.php');
$add = mysqli_query($induction, "INSERT INTO `films`(`nameFilm`,`yearFilm`,`country`,`genre`,`timeFilm`,`descriptionFilm`,`countViews`,`countLikes`,`image`,`page`,`video`) VALUES ('$nameFilm','$yearFilm','$country','$genre','$timeFilm','$descriptionFilm','$countViews','$countLikes','$image','$page','$video')");
header('Location: /pages/admin/admin-panel.php');
