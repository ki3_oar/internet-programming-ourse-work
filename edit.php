<?php
include 'php/connection.php';
use configuration\Config as config;
$induction = new mysqli(config::$ip,config::$name,config::$pass,config::$db_name);
$editedId = $_GET['edit'];

$nameFilm = $_POST['nameFilm'];
$yearFilm = $_POST['yearFilm'];
$country = $_POST['country'];
$genre = $_POST['genre'];
$timeFilm = $_POST['timeFilm'];
$descriptionFilm = $_POST['descriptionFilm'];
if ($_POST['countViews']!='') {
    $countViews = $_POST['countViews'];
} else {
    $countViews = 0;
}
if ($_POST['countLikes']!='') {
    $countLikes = $_POST['countLikes'];
} else {
    $countLikes = 0;
}
$image = '/images/'.$_FILES['imageFilm']['name'];
$video = '/videos/'.$_FILES['videoFilm']['name'];

//загрузка в тимчасову папку temp
move_uploaded_file($_FILES['imageFilm']['tmp_name'], 'temp/'.$_FILES['imageFilm']['name']);
move_uploaded_file($_FILES['videoFilm']['tmp_name'], 'temp/'.$_FILES['videoFilm']['name']);
//картинку вставляємо в папку images, а відео в папку videos
rename('temp/'.$_FILES['imageFilm']['name'],'images/'.$_FILES['imageFilm']['name']);
rename('temp/'.$_FILES['videoFilm']['name'],'videos/'.$_FILES['videoFilm']['name']);

$idit = mysqli_query($induction, "UPDATE `films` SET `nameFilm`='$nameFilm',`yearFilm`='$yearFilm',`country`='$country',`genre`='$genre',`timeFilm`='$timeFilm',`descriptionFilm`='$descriptionFilm',`countViews`='$countViews',`countLikes`='$countLikes',`image`='$image',`video`='$video' WHERE `idFilm`='$editedId'");
header('Location: /pages/admin/admin-panel.php');