<?php
include 'php/connection.php';
use configuration\Config as config;
$induction = new mysqli(config::$ip,config::$name,config::$pass,config::$db_name);
$deleteId = $_GET['delete'];
$getImage = mysqli_query($induction, "SELECT `image` FROM `films` WHERE `idFilm`='$deleteId'");
$getPage = mysqli_query($induction, "SELECT `page` FROM `films` WHERE `idFilm`='$deleteId'");
$getVideo = mysqli_query($induction, "SELECT `video` FROM `films` WHERE `idFilm`='$deleteId'");
$im = mysqli_fetch_assoc($getImage);
$pg = mysqli_fetch_assoc($getPage);
$vi = mysqli_fetch_assoc($getVideo);
$im = 'images/'.substr($im['image'],8);
$pg = 'pages/'.substr($pg['page'],7);
$vi = 'videos/'.substr($vi['video'],8);
unlink($im);
unlink($pg);
unlink($vi);
$del = mysqli_query($induction, "DELETE FROM `films` WHERE `idFilm`='$deleteId'");
header('Location: /pages/admin/admin-panel.php');