<?php
$genre = 'бойовик';
include '../../php/checkViews.php';
include 'genres-params.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Дивитися фільми онлайн</title>
    <!--Іконки різних розширень-->
    <link rel="apple-touch-icon" sizes="57x57" href="../../icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../../icon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../../icon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../../icon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../../icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../../icon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../../icon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../../icon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../../icon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="../../icon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../../icon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../icon/favicon-16x16.png">
    <link rel="manifest" href="../../icon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="icon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!--Підключення всіх css файлів-->
    <link rel="stylesheet" href="../../css/index/preload.css" type="text/css">
    <link rel="stylesheet" href="../../css/index/navbar.css" type="text/css">
    <link rel="stylesheet" href="../../css/index/menu.css" type="text/css">
    <link rel="stylesheet" href="../../css/index/content.css" type="text/css">
    <link rel="stylesheet" href="../../css/index/footer.css" type="text/css">
    <link rel="stylesheet" href="../../css/index/drowdown.css" type="text/css">
    <!--Підключення шрифту-->
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500;700;800;900&display=swap" rel="stylesheet">
    <!--Font Awesome для іконок-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" rel="stylesheet">
</head>
<body>

<div class="header general">
    <nav class="wrapper">
        <div><a href="/" class="logo"><span>Film</span> Fund</a></div>
        <div>
            <form action="/pages/search/index-search.php" method="post">
                <input type="text" placeholder="Назва фільму" name="search" id="search">
                <button type="submit" value="Пошук" class="btn">Пошук</button>
            </form>
        </div>
        <?php if($_COOKIE['user']==''){?>
            <div>
                <button class="btn" onclick="document.location='/pages/authorization.php'">Вхід</button>
                <button class="btn" onclick="document.location='/pages/registration.php'">Реєстрація</button>
            </div>
        <?php } else {?>
            <div class="dropdown">
                <div onclick="myFunction()" onmousedown="return false" class="dropbtn"><?php echo $_COOKIE['user'].' ';?><i class="fas fa-user"></i></div>
                <div id="myDropdown" class="dropdown-content">
                    <a href="/pages/latter/index-latter.php"><i class="fas fa-history"></i> Переглянути пізніше</a>
                    <a href="/pages/star/index-star.php"><i class="fas fa-star"></i> Обране</a>
                    <a href="/php/exit.php"><i class="fas fa-sign-out-alt"></i> Вихід</a>
                </div>
            </div>
        <?php } ?>
    </nav>
    <nav class="menu">
        <ul>
            <li><a href="/pages/genres/vestern.php"><p>Вестерн</p></a></li>
            <li><a href="/pages/genres/boyovik.php"><p>Бойовик</p></a></li>
            <li><a href="/pages/genres/drama.php"><p>Драма</p></a></li>
            <li><a href="/pages/genres/komedia.php"><p>Комедія</p></a></li>
            <li><a href="/pages/genres/melodrama.php"><p>Мелодрама</p></a></li>
            <li><a href="/pages/genres/myltfilm.php"><p>Мультфільм</p></a></li>
            <li><a href="/pages/genres/zhahy.php"><p>Жахи</p></a></li>
            <li><a href="/pages/genres/simeyniy.php"><p>Сімейний</p></a></li>
            <li><a href="/pages/genres/fantastuka.php"><p>Фантастика</p></a></li>
            <li><a href="/pages/genres/truler.php"><p>Трилер</p></a></li>
        </ul>
    </nav>
    <div class="filters">
        <div class="filter-item">Фільтри:</div>
        <select class="filt" onchange="if (this.value) window.location.href=this.value">
            <option value="">Країна</option>
            <option value="/pages/filterCountry/australia.php">Австралія</option>
            <option value="/pages/filterCountry/avstria.php">Австрія</option>
            <option value="/pages/filterCountry/argentina.php">Аргентина</option>
            <option value="/pages/filterCountry/belgia.php">Бельгія</option>
            <option value="/pages/filterCountry/brazilia.php">Бразилія</option>
            <option value="/pages/filterCountry/velykobritain.php">Великобританія</option>
            <option value="/pages/filterCountry/virmenia.php">Вірменія</option>
            <option value="/pages/filterCountry/gonkong.php">Гонконг</option>
            <option value="/pages/filterCountry/greece.php">Греція</option>
            <option value="/pages/filterCountry/dania.php">Данія</option>
            <option value="/pages/filterCountry/izrail.php">Ізраїль</option>
            <option value="/pages/filterCountry/india.php">Індія</option>
            <option value="/pages/filterCountry/indonesia.php">Індонезія</option>
            <option value="/pages/filterCountry/iran.php">Іран</option>
            <option value="/pages/filterCountry/irlandia.php">Ірландія</option>
            <option value="/pages/filterCountry/islandia.php">Ісландія</option>
            <option value="/pages/filterCountry/spain.php">Іспанія</option>
            <option value="/pages/filterCountry/italy.php">Італія</option>
            <option value="/pages/filterCountry/kazahstan.php">Казахстан</option>
            <option value="/pages/filterCountry/canada.php">Канада</option>
            <option value="/pages/filterCountry/china.php">Китай</option>
            <option value="/pages/filterCountry/mehico.php">Мексика</option>
            <option value="/pages/filterCountry/niderlands.php">Нідерланди</option>
            <option value="/pages/filterCountry/germany.php">Німеччина</option>
            <option value="/pages/filterCountry/nova-zelandia.php">Нова Зеландія</option>
            <option value="/pages/filterCountry/norvegia.php">Норвегія</option>
            <option value="/pages/filterCountry/polsha.php">Польша</option>
            <option value="/pages/filterCountry/russia.php">Росія</option>
            <option value="/pages/filterCountry/rumunia.php">Румунія</option>
            <option value="/pages/filterCountry/serbia.php">Сербія</option>
            <option value="/pages/filterCountry/sssr.php">СССР</option>
            <option value="/pages/filterCountry/usa.php">США</option>
            <option value="/pages/filterCountry/tailand.php">Таїланд</option>
            <option value="/pages/filterCountry/turechina.php">Туреччина</option>
            <option value="/pages/filterCountry/ugorshina.php">Угорщина</option>
            <option value="/pages/filterCountry/ukraine.php">Україна</option>
            <option value="/pages/filterCountry/finlandia.php">Фінляндія</option>
            <option value="/pages/filterCountry/france.php">Франція</option>
            <option value="/pages/filterCountry/chehia.php">Чехія</option>
            <option value="/pages/filterCountry/shvecaria.php">Швейцарія</option>
            <option value="/pages/filterCountry/shvecia.php">Швеція</option>
            <option value="/pages/filterCountry/pivdena-korea.php">Південна Корея</option>
            <option value="/pages/filterCountry/japan.php">Японія</option>
        </select>
        <select class="filt" onchange="if (this.value) window.location.href=this.value">
            <option>Рік</option>
            <option value="/pages/filterYear/2020.php">2020</option>
            <option value="/pages/filterYear/2019.php">2019</option>
            <option value="/pages/filterYear/2018.php">2018</option>
            <option value="/pages/filterYear/2017.php">2017</option>
            <option value="/pages/filterYear/2016.php">2016</option>
            <option value="/pages/filterYear/2015.php">2015</option>
            <option value="/pages/filterYear/2014.php">2014</option>
            <option value="/pages/filterYear/2013.php">2013</option>
            <option value="/pages/filterYear/2012.php">2012</option>
            <option value="/pages/filterYear/2011.php">2011</option>
            <option value="/pages/filterYear/2010.php">2010</option>
            <option value="/pages/filterYear/2009.php">2009</option>
            <option value="/pages/filterYear/2008.php">2008</option>
            <option value="/pages/filterYear/2007.php">2007</option>
            <option value="/pages/filterYear/2006.php">2006</option>
            <option value="/pages/filterYear/2005.php">2005</option>
            <option value="/pages/filterYear/2004.php">2004</option>
            <option value="/pages/filterYear/2003.php">2003</option>
            <option value="/pages/filterYear/2002.php">2002</option>
            <option value="/pages/filterYear/2001.php">2001</option>
            <option value="/pages/filterYear/2000.php">2000</option>
            <option value="/pages/filterYear/1999.php">1999</option>
            <option value="/pages/filterYear/1998.php">1998</option>
            <option value="/pages/filterYear/1997.php">1997</option>
            <option value="/pages/filterYear/1996.php">1996</option>
            <option value="/pages/filterYear/1995.php">1995</option>
            <option value="/pages/filterYear/1994.php">1994</option>
            <option value="/pages/filterYear/1993.php">1993</option>
            <option value="/pages/filterYear/1992.php">1992</option>
            <option value="/pages/filterYear/1991.php">1991</option>
            <option value="/pages/filterYear/1990.php">1990</option>
            <option value="/pages/filterYear/1989.php">1989</option>
            <option value="/pages/filterYear/1988.php">1988</option>
            <option value="/pages/filterYear/1987.php">1987</option>
            <option value="/pages/filterYear/1986.php">1986</option>
            <option value="/pages/filterYear/1985.php">1985</option>
            <option value="/pages/filterYear/1984.php">1984</option>
            <option value="/pages/filterYear/1983.php">1983</option>
            <option value="/pages/filterYear/1982.php">1982</option>
            <option value="/pages/filterYear/1981.php">1981</option>
            <option value="/pages/filterYear/1980.php">1980</option>
            <option value="/pages/filterYear/1979.php">1979</option>
            <option value="/pages/filterYear/1978.php">1978</option>
            <option value="/pages/filterYear/1977.php">1977</option>
            <option value="/pages/filterYear/1976.php">1976</option>
            <option value="/pages/filterYear/1975.php">1975</option>
            <option value="/pages/filterYear/1974.php">1974</option>
            <option value="/pages/filterYear/1973.php">1973</option>
            <option value="/pages/filterYear/1972.php">1972</option>
            <option value="/pages/filterYear/1971.php">1971</option>
            <option value="/pages/filterYear/1970.php">1970</option>
            <option value="/pages/filterYear/1969.php">1969</option>
            <option value="/pages/filterYear/1968.php">1968</option>
            <option value="/pages/filterYear/1967.php">1967</option>
            <option value="/pages/filterYear/1966.php">1966</option>
            <option value="/pages/filterYear/1965.php">1965</option>
            <option value="/pages/filterYear/1964.php">1964</option>
            <option value="/pages/filterYear/1963.php">1963</option>
            <option value="/pages/filterYear/1962.php">1962</option>
            <option value="/pages/filterYear/1961.php">1961</option>
            <option value="/pages/filterYear/1960.php">1960</option>
            <option value="/pages/filterYear/1959.php">1959</option>
            <option value="/pages/filterYear/1958.php">1958</option>
            <option value="/pages/filterYear/1957.php">1957</option>
            <option value="/pages/filterYear/1956.php">1956</option>
            <option value="/pages/filterYear/1955.php">1955</option>
            <option value="/pages/filterYear/1954.php">1954</option>
            <option value="/pages/filterYear/1953.php">1953</option>
            <option value="/pages/filterYear/1952.php">1952</option>
            <option value="/pages/filterYear/1951.php">1951</option>
            <option value="/pages/filterYear/1950.php">1950</option>
        </select>
        <div class="filt" id="popular">Найпопулярніші</div>
    </div>

    <div class="content general">
        <div class="filter-name"><em><span class="red">&#8212</span> Жанр: Бойовик</em></div>
        <?php
        if ($countRow>0) {
        for(;$countRow>0;$countRow--) { ?>
        <div class="row">
            <?php $article=mysqli_fetch_assoc($result); ?>
                <a class="block" href="<?php echo $article['page']; ?>">
                    <div class="image"><img src="<?php echo $article['image'];?>" alt=""></div>
                    <div class="desc"><?php echo $article['nameFilm'];?></div>
                    <div class="marks">
                        <p><i class="far fa-eye icon"><?php echo ' '; echo Check($article['countViews']);?></i></p>
                        <p><i class="far fa-heart icon"><?php echo ' '; echo Check($article['countLikes']);?></i></p>
                        <?php
                        $countCom = getCountCom($article['idFilm'], $induction);
                        echo '<p><i class="icon far fa-comment"> '.implode($countCom).'</i></p>'; ?>
                    </div>
                </a>
                <?php $article=mysqli_fetch_assoc($result); ?>
                <a class="block" href="<?php echo $article['page']; ?>">
                    <div class="image"><img src="<?php echo $article['image'];?>" alt=""></div>
                    <div class="desc"><?php echo $article['nameFilm'];?></div>
                    <div class="marks">
                        <p><i class="far fa-eye icon"><?php echo ' '; echo Check($article['countViews']);?></i></p>
                        <p><i class="far fa-heart icon"><?php echo ' '; echo Check($article['countLikes']);?></i></p>
                        <?php
                        $countCom = getCountCom($article['idFilm'], $induction);
                        echo '<p><i class="icon far fa-comment"> '.implode($countCom).'</i></p>'; ?>
                    </div>
                </a>
                <?php $article=mysqli_fetch_assoc($result); ?>
                <a class="block" href="<?php echo $article['page']; ?>">
                    <div class="image"><img src="<?php echo $article['image'];?>" alt=""></div>
                    <div class="desc"><?php echo $article['nameFilm'];?></div>
                    <div class="marks">
                        <p><i class="far fa-eye icon"><?php echo ' '; echo Check($article['countViews']);?></i></p>
                        <p><i class="far fa-heart icon"><?php echo ' '; echo Check($article['countLikes']);?></i></p>
                        <?php
                        $countCom = getCountCom($article['idFilm'], $induction);
                        echo '<p><i class="icon far fa-comment"> '.implode($countCom).'</i></p>'; ?>
                    </div>
                </a>
                <?php $article=mysqli_fetch_assoc($result); ?>
                <a class="block" href="<?php echo $article['page']; ?>">
                    <div class="image"><img src="<?php echo $article['image'];?>" alt=""></div>
                    <div class="desc"><?php echo $article['nameFilm'];?></div>
                    <div class="marks">
                        <p><i class="far fa-eye icon"><?php echo ' '; echo Check($article['countViews']);?></i></p>
                        <p><i class="far fa-heart icon"><?php echo ' '; echo Check($article['countLikes']);?></i></p>
                        <?php
                        $countCom = getCountCom($article['idFilm'], $induction);
                        echo '<p><i class="icon far fa-comment"> '.implode($countCom).'</i></p>'; ?>
                    </div>
                </a>
        </div>
        <?php } ?>
            <?php if ($countAll>16) { ?>
                <div class="button" onclick="anime()">
                    <div class="load-more">Показати більше
                        <div id="hellopreloader">
                            <div id="hellopreloader_preload"></div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>

        <?php if ($countBlock==1) { ?>
            <div class="row">
                <?php $article=mysqli_fetch_assoc($result); ?>
                <a class="block" href="<?php echo $article['page']; ?>">
                    <div class="image"><img src="<?php echo $article['image'];?>" alt=""></div>
                    <div class="desc"><?php echo $article['nameFilm'];?></div>
                    <div class="marks">
                        <p><i class="far fa-eye icon"><?php echo ' '; echo Check($article['countViews']);?></i></p>
                        <p><i class="far fa-heart icon"><?php echo ' '; echo Check($article['countLikes']);?></i></p>
                        <?php
                        $countCom = getCountCom($article['idFilm'], $induction);
                        echo '<p><i class="icon far fa-comment"> '.implode($countCom).'</i></p>'; ?>
                    </div>
                </a>
                <a class="block hidden">
                    <div class="image"><img src="" alt=""></div>
                    <div class="desc"></div>
                    <div class="marks">
                        <p><i class="far fa-eye icon"></i></p>
                        <p><i class="far fa-heart icon"></i></p>
                    </div>
                </a>
                <a class="block hidden">
                    <div class="image"><img src="" alt=""></div>
                    <div class="desc"></div>
                    <div class="marks">
                        <p><i class="far fa-eye icon"></i></p>
                        <p><i class="far fa-heart icon"></i></p>
                    </div>
                </a>
                <a class="block hidden">
                    <div class="image"><img src="" alt=""></div>
                    <div class="desc"></div>
                    <div class="marks">
                        <p><i class="far fa-eye icon"></i></p>
                        <p><i class="far fa-heart icon"></i></p>
                    </div>
                </a>
            </div>
        <?php } ?>

        <?php if ($countBlock==2) { ?>
            <div class="row">
                <?php $article=mysqli_fetch_assoc($result); ?>
                <a class="block" href="<?php echo $article['page']; ?>">
                    <div class="image"><img src="<?php echo $article['image'];?>" alt=""></div>
                    <div class="desc"><?php echo $article['nameFilm'];?></div>
                    <div class="marks">
                        <p><i class="far fa-eye icon"><?php echo ' '; echo Check($article['countViews']);?></i></p>
                        <p><i class="far fa-heart icon"><?php echo ' '; echo Check($article['countLikes']);?></i></p>
                        <?php
                        $countCom = getCountCom($article['idFilm'], $induction);
                        echo '<p><i class="icon far fa-comment"> '.implode($countCom).'</i></p>'; ?>
                    </div>
                </a>
                <?php $article=mysqli_fetch_assoc($result); ?>
                <a class="block" href="<?php echo $article['page']; ?>">
                    <div class="image"><img src="<?php echo $article['image'];?>" alt=""></div>
                    <div class="desc"><?php echo $article['nameFilm'];?></div>
                    <div class="marks">
                        <p><i class="far fa-eye icon"><?php echo ' '; echo Check($article['countViews']);?></i></p>
                        <p><i class="far fa-heart icon"><?php echo ' '; echo Check($article['countLikes']);?></i></p>
                        <?php
                        $countCom = getCountCom($article['idFilm'], $induction);
                        echo '<p><i class="icon far fa-comment"> '.implode($countCom).'</i></p>'; ?>
                    </div>
                </a>
                <a class="block hidden">
                    <div class="image"><img src="" alt=""></div>
                    <div class="desc"></div>
                    <div class="marks">
                        <p><i class="far fa-eye icon"></i></p>
                        <p><i class="far fa-heart icon"></i></p>
                    </div>
                </a>
                <a class="block hidden">
                    <div class="image"><img src="" alt=""></div>
                    <div class="desc"></div>
                    <div class="marks">
                        <p><i class="far fa-eye icon"></i></p>
                        <p><i class="far fa-heart icon"></i></p>
                    </div>
                </a>
            </div>
        <?php } ?>

        <?php if ($countBlock==3) { ?>
            <div class="row">
                <?php $article=mysqli_fetch_assoc($result); ?>
                <a class="block" href="<?php echo $article['page']; ?>">
                    <div class="image"><img src="<?php echo $article['image'];?>" alt=""></div>
                    <div class="desc"><?php echo $article['nameFilm'];?></div>
                    <div class="marks">
                        <p><i class="far fa-eye icon"><?php echo ' '; echo Check($article['countViews']);?></i></p>
                        <p><i class="far fa-heart icon"><?php echo ' '; echo Check($article['countLikes']);?></i></p>
                        <?php
                        $countCom = getCountCom($article['idFilm'], $induction);
                        echo '<p><i class="icon far fa-comment"> '.implode($countCom).'</i></p>'; ?>
                    </div>
                </a>
                <?php $article=mysqli_fetch_assoc($result); ?>
                <a class="block" href="<?php echo $article['page']; ?>">
                    <div class="image"><img src="<?php echo $article['image'];?>" alt=""></div>
                    <div class="desc"><?php echo $article['nameFilm'];?></div>
                    <div class="marks">
                        <p><i class="far fa-eye icon"><?php echo ' '; echo Check($article['countViews']);?></i></p>
                        <p><i class="far fa-heart icon"><?php echo ' '; echo Check($article['countLikes']);?></i></p>
                        <?php
                        $countCom = getCountCom($article['idFilm'], $induction);
                        echo '<p><i class="icon far fa-comment"> '.implode($countCom).'</i></p>'; ?>
                    </div>
                </a>
                <?php $article=mysqli_fetch_assoc($result); ?>
                <a class="block" href="<?php echo $article['page']; ?>">
                    <div class="image"><img src="<?php echo $article['image'];?>" alt=""></div>
                    <div class="desc"><?php echo $article['nameFilm'];?></div>
                    <div class="marks">
                        <p><i class="far fa-eye icon"><?php echo ' '; echo Check($article['countViews']);?></i></p>
                        <p><i class="far fa-heart icon"><?php echo ' '; echo Check($article['countLikes']);?></i></p>
                        <?php
                        $countCom = getCountCom($article['idFilm'], $induction);
                        echo '<p><i class="icon far fa-comment"> '.implode($countCom).'</i></p>'; ?>
                    </div>
                </a>
                <a class="block hidden">
                    <div class="image"><img src="" alt=""></div>
                    <div class="desc"></div>
                    <div class="marks">
                        <p><i class="far fa-eye icon"></i></p>
                        <p><i class="far fa-heart icon"></i></p>
                    </div>
                </a>
            </div>
        <?php } ?>
    </div>
</div>
<div class="footer general">
    <div id="sign" class="float"><a href="#top" id="home">©&nbsp Film Fund</a></div>
    <div class="social float">
        <a href="https://www.instagram.com/?hl=ru" class="social-item"><i class="fab fa-instagram"></i></a>
        <a href="https://twitter.com/" class="social-item"><i class="fab fa-twitter"></i></a>
        <a href="https://www.facebook.com/" class="social-item"><i class="fab fa-facebook-f"></i></a>
        <a href="https://www.viber.com/" class="social-item"><i class="fab fa-viber"></i></a>
        <a href="https://web.telegram.org/" class="social-item"><i class="fab fa-telegram"></i></a>
    </div>
    <div class="clear:both;"></div>
</div>

<script src="../../js/dropdown.js"></script>
<script>var num = Number('<?php echo $countAll?>');</script>
<script src="../../js/preload.js"></script>
<script src="../../js/index.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</body>
</html>
