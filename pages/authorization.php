<?php
setcookie('error','smth',time()-3600,'/');
setcookie('wrongLogin','login',time()-3600,'/');
setcookie('wrongPass','pass',time()-3600,'/');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Форма регістрації</title>
    <!--Іконки різних розширень-->
    <link rel="apple-touch-icon" sizes="57x57" href="../icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../icon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../icon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../icon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../icon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../icon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../icon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../icon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="../icon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../icon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../icon/favicon-16x16.png">
    <link rel="manifest" href="../icon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="icon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="../css/registration/style.css">
    <link rel="stylesheet" href="../css/index/content.css">
    <link rel="stylesheet" href="../css/index/navbar.css">
    <link rel="stylesheet" href="../css/index/footer.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" rel="stylesheet">
</head>
<body>
<div class="header general">
    <nav class="wrapper">
        <div><a href="/" class="logo"><span>Film</span> Fund</a></div>
        <div>
            <form action="/pages/search/index-search.php" method="post">
                <input type="text" placeholder="Назва фільму" name="search" id="search">
                <button type="submit" value="Пошук" class="btn">Пошук</button>
            </form>
        </div>
        <div><button class="btn">Вхід</button><button class="btn" onclick="document.location='/pages/registration.php'">Реєстрація</button></div>
    </nav>
    <div class="content general">
        <div class="cont">
            <h1 style="color: #0e8393;">Форма авторизації</h1>
            <form action="/php/auth.php" method="post">
                <input type="text" class="form-control" name="login" id="login" placeholder="Введіть логін" onkeyup="checkLogPass()"><br>
                <input type="password" class="form-control" name="pass" id="pass" placeholder="Введіть пароль" onkeyup="checkLogPass()"><br>
                <input type="checkbox" name="checkAdmin" value="Yes">Авторизуватись як адмін<br>
                <button class="btn-success" type="submit">Авторизуватися</button>
                <?php
                if (!isset($_COOKIE['wrongLogin']) or !isset($_COOKIE['wrongPass'])) {
                if (isset($_COOKIE['error'])) { ?>
                    <div class="message">Неправильний логін або пароль!</div>
                <?php } }
                if (isset($_COOKIE['wrongLogin'])) { ?>
                    <div class="message">Ви не ввели логін!</div>
                <?php }
                if (isset($_COOKIE['wrongPass'])) { ?>
                    <div class="message">Ви не ввели пароль!</div>
                <?php }
                if (isset($_COOKIE['error-admin'])) { ?>
                    <div class="message">Неправильний логін або пароль адміністратора!</div>
                <?php } ?>

            </form>
        </div>
    </div>
</div>
<div class="footer general">
    <div id="sign" class="float"><a href="#top" id="home">©&nbsp Film Fund</a></div>
    <div class="social float">
        <a href="https://www.instagram.com/?hl=ru" class="social-item"><i class="fab fa-instagram"></i></a>
        <a href="https://twitter.com/" class="social-item"><i class="fab fa-twitter"></i></a>
        <a href="https://www.facebook.com/" class="social-item"><i class="fab fa-facebook-f"></i></a>
        <a href="https://www.viber.com/" class="social-item"><i class="fab fa-viber"></i></a>
        <a href="https://web.telegram.org/" class="social-item"><i class="fab fa-telegram"></i></a>
    </div>
    <div class="clear:both;"></div>
</div>

<script>
    var btn = document.getElementsByClassName('btn-success')[0];

    var pass = document.getElementById('pass');
    var log = document.getElementById('login');
    function checkLogPass(){
        if (pass.value.length>0 && log.value.length>0){
            btn.style.background='#0e8393';
        }
    }
</script>
</body>
</html>