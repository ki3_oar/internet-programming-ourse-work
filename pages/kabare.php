<?php
include '../php/setlikes.php';
include '../php/connection.php';
use configuration\Config as config;
$induction = mysqli_connect(config::$ip,config::$name,config::$pass,config::$db_name);
//з допомогою поточного директорію шукаємо фільм в БД
$path = '/pages/'.substr(__FILE__,41);
$id_query = mysqli_query($induction,"SELECT `idFilm` FROM `films` WHERE `page`='$path'");
$id_article = mysqli_fetch_assoc($id_query);
$id = $id_article['idFilm'];
$result = mysqli_query($induction,"SELECT * FROM `films` WHERE `idFilm`= '$id'");
$comments = mysqli_query($induction, "SELECT * FROM `comments` WHERE `idFilm`='$id' order by `datePublication` desc");
$user = mysqli_query($induction,"SELECT `login` FROM `users`,`comments` WHERE `loginAuthor`=`login`");
$article = mysqli_fetch_assoc($result);
$countComments = mysqli_num_rows($comments);
$numCom = mysqli_num_rows($comments);
//читаємо інформацію про лайки
$array = setLikes($induction, $id, $path);
$liked = $array[0];
$latter = $array[1];
$star = $array[2];
//додавання коментаря
if (isset($_POST['send'])){
    $text=htmlspecialchars($_POST['comment']);
    if (strlen($text)>=4) {
        $user_login = $_COOKIE['user'];
        $addComment = mysqli_query($induction, "INSERT INTO `comments`(`loginAuthor`,`textComment`,`idFilm`) VALUES ('$user_login','$text','$id')");
        header('Refresh: 0');
    }
}
//лічильник переглядів
if (isset($_POST['btn-like']) or isset($_POST['btn-latter']) or isset($_POST['btn-star'])){
    $updateViews = mysqli_query($induction, "UPDATE `films` SET `countViews`=`countViews`-1 WHERE `idFilm`='$id'");
}
if (!isset($_POST['btn-latter-avtorizated']) and !isset($_POST['btn-like-avtorizated']) and !isset($_POST['btn-star-avtorizated']) and !isset($_POST['send'])){
    $updateViews = mysqli_query($induction, "UPDATE `films` SET `countViews`=`countViews`+1 WHERE `idFilm`='$id'");
} else {
    $updateViews = mysqli_query($induction, "UPDATE `films` SET `countViews`=`countViews`-1 WHERE `idFilm`='$id'");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Дивитися фільми онлайн</title>
    <!--Іконки різних розширень-->
    <link rel="apple-touch-icon" sizes="57x57" href="../icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../icon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../icon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../icon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../icon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../icon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../icon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../icon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="../icon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../icon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../icon/favicon-16x16.png">
    <link rel="manifest" href="../icon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="icon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!--Підключення всіх css файлів-->
    <link rel="stylesheet" href="../css/index/preload.css" type="text/css">
    <link rel="stylesheet" href="../css/index/navbar.css" type="text/css">
    <link rel="stylesheet" href="../css/index/menu.css" type="text/css">
    <link rel="stylesheet" href="../css/index-single/content.css" type="text/css">
    <link rel="stylesheet" href="../css/index/footer.css" type="text/css">
    <link rel="stylesheet" href="../css/index/drowdown.css" type="text/css">
    <!--Підключення шрифту-->
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500;700;800;900&display=swap" rel="stylesheet">
    <!--Font Awesome для іконок-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" rel="stylesheet">
    <!--Video player-->
    <link rel="stylesheet" href="https://cdn.plyr.io/3.5.6/plyr.css"/>
</head>
<body>

<div class="header general">
    <nav class="wrapper">
        <div><a href="/" class="logo"><span>Film</span> Fund</a></div>
        <div>
            <form action="/pages/search/index-search.php" method="post">
                <input type="text" placeholder="Назва фільму" name="search" id="search">
                <button type="submit" value="Пошук" class="btn">Пошук</button>
            </form>
        </div>
        <?php if($_COOKIE['user']==''){?>
            <div>
                <button class="btn" onclick="document.location='/pages/authorization.php'">Вхід</button>
                <button class="btn" onclick="document.location='/pages/registration.php'">Реєстрація</button>
            </div>
        <?php } else {?>
            <div class="dropdown">
                <div onclick="myFunction()" onmousedown="return false" class="dropbtn"><?php echo $_COOKIE['user'].' ';?><i class="fas fa-user"></i></div>
                <div id="myDropdown" class="dropdown-content">
                    <a href="/pages/latter/index-latter.php"><i class="fas fa-history"></i> Переглянути пізніше</a>
                    <a href="/pages/star/index-star.php"><i class="fas fa-star"></i> Обране</a>
                    <a href="../../php/exit.php"><i class="fas fa-sign-out-alt"></i> Вихід</a>
                </div>
            </div>
        <?php } ?>
    </nav>
    <nav class="menu">
        <ul>
            <li><a href="/pages/genres/vestern.php"><p>Вестерн</p></a></li>
            <li><a href="/pages/genres/boyovik.php"><p>Бойовик</p></a></li>
            <li><a href="/pages/genres/drama.php"><p>Драма</p></a></li>
            <li><a href="/pages/genres/komedia.php"><p>Комедія</p></a></li>
            <li><a href="/pages/genres/melodrama.php"><p>Мелодрама</p></a></li>
            <li><a href="/pages/genres/myltfilm.php"><p>Мультфільм</p></a></li>
            <li><a href="/pages/genres/zhahy.php"><p>Жахи</p></a></li>
            <li><a href="/pages/genres/simeyniy.php"><p>Сімейний</p></a></li>
            <li><a href="/pages/genres/fantastuka.php"><p>Фантастика</p></a></li>
            <li><a href="/pages/genres/truler.php"><p>Трилер</p></a></li>
        </ul>
    </nav>

    <div class="content general">
        <div class="flex">
            <div class="image"><img src="<?php echo $article['image']; ?>"></div>
            <div class="short-description">
                <div class="desc-row">
                    <div class="r">назва</div>
                    <div class="r2"><?php echo $article['nameFilm']; ?></div>
                </div>
                <div class="desc-row">
                    <div class="r gray">рік</div>
                    <div class="r2 gray"><?php echo $article['yearFilm']; ?></div>
                </div>
                <div class="desc-row">
                    <div class="r">країна</div>
                    <div class="r2"><?php echo $article['country']; ?></div>
                </div>
                <div class="desc-row">
                    <div class="r gray">жанр</div>
                    <div class="r2 gray"><?php echo $article['genre']; ?></div>
                </div>
                <div class="desc-row">
                    <div class="r">час</div>
                    <div class="r2"><?php echo $article['timeFilm']; echo ' хв.' ?></div>
                </div>

                <div class="marks desc-row gray">
                    <?php if ($_COOKIE['user']!='') { ?>
                    <form class="vnyt" method="post">
                        <button type="submit" class="block flag" disabled="disabled"><i class="far fa-eye"><?php echo ' '; echo $article['countViews']; ?></i></button>
                        <button type="submit" onclick="addLike()" style="<?php if ($liked) echo 'color: lightblue'; else echo 'color: gray'; ?>" class="block flag" name="btn-like-avtorizated" id="block-like"><i id="like" onmousedown="return false" onclick="addLike()" class="<?php if ($liked) echo 'fas'; else echo 'far'; ?> fa-heart"><span class="slim"><?php echo ' '; echo $article['countLikes'];?></span></i></button>
                        <button type="submit" style="<?php if ($latter) echo 'color: lightblue'; else echo 'color: gray'; ?>" class="flag" name="btn-latter-avtorizated"><i class="fas fa-history"></i> На потім</button>
                        <button type="submit" style="<?php if ($star) echo 'color: lightblue'; else echo 'color: gray';?>" class="flag" name="btn-star-avtorizated"><i class="fas fa-star"></i> В обране</button>
                    </form>
                    <? } else { ?>
                    <form class="vnyt" method="post">
                        <button type="submit" class="block flag" disabled="disabled"><i class="far fa-eye"><?php echo ' '; echo $article['countViews']; ?></i></button>
                        <button type="submit" class="block flag" name="btn-like" id="block-like" onclick="alertMess()"><i id="like" onmousedown="return false" class="far fa-heart"><span class="slim"><?php echo ' '; echo $article['countLikes'];?></span></i></button>
                        <button type="submit" class="flag" name="btn-latter" onclick="addLatter()"><i class="fas fa-history"></i> На потім</button>
                        <button type="submit" class="flag" name="btn-star" onclick="addStar()"><i class="fas fa-star"></i> В обране</button>
                    </form>
                    <?php } ?>
                </div>
            </div>
        </div>

        <div class="description"><div>Опис:</div>
            <div><span class="white"><?php echo $article['descriptionFilm'];?></span></div>
        </div>

        <video poster="../images/poster.jpg" id="player" playsinline controls>
            <source src="<?php echo $article['video']; ?>" type="video/mp4"/>
        </video>

        <?php if($_COOKIE['user']!='') { ?>
            <div id="addComment">
                <form class="insideAddComment" method="post">
                    <div id="outsideArea"><textarea style="resize: none" name="comment" id="textarea" maxlength="1000" placeholder="Ваш коментар"></textarea></div>
                    <button class="btn" name="send" id="send" type="submit">Відправити</button>
            </div>
        <?php } ?>
        <div id="comments">
            <div class="com"><span class="com"><span class="red">К</span>оментарі</span></div>
            <?php if($countComments==0) { ?>
                <div>Коментарі відсутні!</div>
            <?php } else { ?>
                <?php for (;$countComments>0;$countComments--) {
                    $comment=mysqli_fetch_assoc($comments);
                    $user_login = mysqli_fetch_assoc($user);?>
                    <div class="comment-block">
                        <div class="authour-row">
                            <div><i class="far fa-user"></i><?php echo " ".$user_login['login']; ?></div>
                            <div><? echo $comment['datePublication']; ?></div>
                        </div>
                        <div class="main-comment">
                            <div class="avatar-block"><i class="fas fa-user-tie avatar"></i></div>
                            <div class="comment-text"><?php echo $comment['textComment']; ?></div>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($numCom>10) { ?>
                    <div class="button" onclick="anime()">
                        <div class="load-more">Більше коментарів
                            <div id="hellopreloader">
                                <div id="hellopreloader_preload"></div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>
<div class="footer general">
    <div id="sign" class="float"><a href="#top" id="home">©&nbsp Film Fund</a></div>
    <div class="social float">
        <a href="https://www.instagram.com/?hl=ru" class="social-item"><i class="fab fa-instagram"></i></a>
        <a href="https://twitter.com/" class="social-item"><i class="fab fa-twitter"></i></a>
        <a href="https://www.facebook.com/" class="social-item"><i class="fab fa-facebook-f"></i></a>
        <a href="https://www.viber.com/" class="social-item"><i class="fab fa-viber"></i></a>
        <a href="https://web.telegram.org/" class="social-item"><i class="fab fa-telegram"></i></a>
    </div>
    <div class="clear:both;"></div>
</div>


<script>
    function alertMess() {
        alert("Для оцінювання фільму необхідно авторизуватися!");
    }
    function addStar(){
        alert("Щоб додати фільм до списку \"Обрані\" необхідно авторизуватися!")
    }
    function addLatter(){
        alert("Щоб додати фільм до списку \"Переглянути пізніше\" необхідно авторизуватися!")
    }
</script>
<script src="../js/dropdown.js"></script>
<script>var count = Number(<?php echo $numCom?>);</script>
<script src="../js/index-single.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

</body>
</html>

