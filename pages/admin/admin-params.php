<?php
use configuration\Config as config;
include '../../php/connection.php';
$induction = mysqli_connect(config::$ip,config::$name,config::$pass,config::$db_name);
$films = mysqli_query($induction, "SELECT * FROM `films`");
$count = mysqli_num_rows($films);

$film1 = mysqli_query($induction, "SELECT COUNT(*) FROM films WHERE datePublication>'2020-07-01' and datePublication<'2020-07-31'");
$f1 = mysqli_fetch_row($film1);
$film2 = mysqli_query($induction, "SELECT COUNT(*) FROM films WHERE datePublication>'2020-08-01' and datePublication<'2020-08-31'");
$f2 = mysqli_fetch_assoc($film2);
$film3 = mysqli_query($induction, "SELECT COUNT(*) FROM films WHERE datePublication>'2020-09-01' and datePublication<'2020-09-30'");
$f3 = mysqli_fetch_assoc($film3);
$film4 = mysqli_query($induction, "SELECT COUNT(*) FROM films WHERE datePublication>'2020-10-01' and datePublication<'2020-10-31'");
$f4 = mysqli_fetch_assoc($film4);
$film5 = mysqli_query($induction, "SELECT COUNT(*) FROM films WHERE datePublication>'2020-11-01' and datePublication<'2020-11-30'");
$f5 = mysqli_fetch_assoc($film5);
$film6 = mysqli_query($induction, "SELECT COUNT(*) FROM films WHERE datePublication>'2020-12-01' and datePublication<'2020-12-31'");
$f6 = mysqli_fetch_assoc($film6);

$com1 = mysqli_query($induction, "SELECT COUNT(*) FROM comments WHERE datePublication>'2020-07-01' and datePublication<'2020-07-31'");
$c1 = mysqli_fetch_assoc($com1);
$com2 = mysqli_query($induction, "SELECT COUNT(*) FROM comments WHERE datePublication>'2020-08-01' and datePublication<'2020-08-31'");
$c2 = mysqli_fetch_assoc($com2);
$com3 = mysqli_query($induction, "SELECT COUNT(*) FROM comments WHERE datePublication>'2020-09-01' and datePublication<'2020-09-30'");
$c3 = mysqli_fetch_assoc($com3);
$com4 = mysqli_query($induction, "SELECT COUNT(*) FROM comments WHERE datePublication>'2020-10-01' and datePublication<'2020-10-31'");
$c4 = mysqli_fetch_assoc($com4);
$com5 = mysqli_query($induction, "SELECT COUNT(*) FROM comments WHERE datePublication>'2020-11-01' and datePublication<'2020-11-30'");
$c5 = mysqli_fetch_assoc($com5);
$com6 = mysqli_query($induction, "SELECT COUNT(*) FROM comments WHERE datePublication>'2020-12-01' and datePublication<'2020-12-31'");
$c6 = mysqli_fetch_assoc($com6);

$user1 = mysqli_query($induction, "SELECT COUNT(*) FROM users WHERE dateCreation>'2020-07-01' and dateCreation<'2020-07-31'");
$u1 = mysqli_fetch_assoc($user1);
$user2 = mysqli_query($induction, "SELECT COUNT(*) FROM users WHERE dateCreation>'2020-08-01' and dateCreation<'2020-08-31'");
$u2 = mysqli_fetch_assoc($user2);
$user3 = mysqli_query($induction, "SELECT COUNT(*) FROM users WHERE dateCreation>'2020-09-01' and dateCreation<'2020-09-30'");
$u3 = mysqli_fetch_assoc($user3);
$user4 = mysqli_query($induction, "SELECT COUNT(*) FROM users WHERE dateCreation>'2020-10-01' and dateCreation<'2020-10-31'");
$u4 = mysqli_fetch_assoc($user4);
$user5 = mysqli_query($induction, "SELECT COUNT(*) FROM users WHERE dateCreation>'2020-11-01' and dateCreation<'2020-11-30'");
$u5 = mysqli_fetch_assoc($user5);
$user6 = mysqli_query($induction, "SELECT COUNT(*) FROM users WHERE dateCreation>'2020-12-01' and dateCreation<'2020-12-31'");
$u6 = mysqli_fetch_assoc($user6);
