<?php
include '../../php/connection.php';
use configuration\Config as config;
$induction = new mysqli(config::$ip,config::$name,config::$pass,config::$db_name);
?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>Дивитися фільми онлайн</title>
    <!--Font Awesome-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" rel="stylesheet">
    <!--Стилі-->
    <link href="../../css/admin-panel/edit-style.css" rel="stylesheet">
    <!--Шрифт-->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
</head>
<body>
<div class="container">
    <form class="form" action="../../upload.php" enctype="multipart/form-data" method="post">
    <h1>Заповніть форму</h1>
    <table>
        <tr>
            <th>Назва фільму:<span class="red"> *</span></th>
        <th><input type="text" class="text" name="nameFilm" value=""></th>
        </tr>
        <tr>
            <th>Рік фільму:<span class="red"> *</span></th>
            <th><input type="text" class="text" name="yearFilm" value=""></th>
        </tr>
        <tr>
            <th>Країна:<span class="red"> *</span></th>
            <th><input type="text" class="text" name="country" value=""></th>
        </tr>
        <tr>
            <th>Жанр:<span class="red"> *</span></th>
            <th><input type="text" class="text" name="genre" value=""></th>
        </tr>
        <tr>
            <th>Тривалість фільму: (в хв.)<span class="red"> *</span></th>
            <th><input type="text" class="text" name="timeFilm" value=""></th>
        </tr>
        <tr>
            <th>Опис фільму:<span class="red"> *</span></th>
            <th><textarea maxlength="1000" name="descriptionFilm" class="desc"></textarea></th>
        </tr>
        <tr>
            <th>Кількість переглядів:</th>
            <th><input type="text" class="text" name="countViews" value=""></th>
        </tr>
        <tr>
            <th>Кількість лайків:</th>
            <th><input type="text" class="text" name="countLikes" value=""></th>
        </tr>
        <tr>
            <th>Постер:<span class="red"> *</span></th>
            <th><input type="file" name="imageFilm" value="" accept="image/jpeg"></th>
        </tr>
        <tr>
            <th>Відео:<span class="red"> *</span></th>
            <th><input type="file" name="videoFilm" accept="video/mp4" value=""></th>
        </tr>
    </table>
        <em><strong><span class="red">*</span> - обов'язкові</strong></em>
    <input type="submit" name="save" value="Додати" class="btn-blue">
    </form>
</div>
</body>

