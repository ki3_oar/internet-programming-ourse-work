<?php
include '../../php/connection.php';
use configuration\Config as config;
$induction = new mysqli(config::$ip,config::$name,config::$pass,config::$db_name);
$redactId = $_GET['redact'];
$red = mysqli_query($induction, "SELECT * FROM `films` WHERE `idFilm`='$redactId'");
$redact = mysqli_fetch_assoc($red);
?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>Дивитися фільми онлайн</title>
    <!--Font Awesome-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" rel="stylesheet">
    <!--Стилі-->
    <link href="../../css/admin-panel/edit-style.css" rel="stylesheet">
    <!--Шрифт-->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
</head>
<body>
<div class="container">
    <form class="form" action="../../edit.php?<?php echo 'edit='.$redact['idFilm']; ?>" enctype="multipart/form-data" method="post">
    <h1>Заповніть форму</h1>
    <table>
        <tr>
            <th>Назва фільму:<span class="red"> *</span></th>
        <th><input type="text" class="text" name="nameFilm" value="<?php echo $redact['nameFilm'] ?>"></th>
        </tr>
        <tr>
            <th>Рік фільму:<span class="red"> *</span></th>
            <th><input type="text" class="text" name="yearFilm" value="<? echo $redact['yearFilm'] ?>"></th>
        </tr>
        <tr>
            <th>Країна:<span class="red"> *</span></th>
            <th><input type="text" class="text" name="country" value="<?php echo $redact['country'] ?>"></th>
        </tr>
        <tr>
            <th>Жанр:<span class="red"> *</span></th>
            <th><input type="text" class="text" name="genre" value="<?php echo $redact['genre'] ?>"></th>
        </tr>
        <tr>
            <th>Тривалість фільму: (в хв.)<span class="red"> *</span></th>
            <th><input type="text" class="text" name="timeFilm" value="<?php echo $redact['timeFilm'] ?>"></th>
        </tr>
        <tr>
            <th>Опис фільму:<span class="red"> *</span></th>
            <th><textarea maxlength="1000" name="descriptionFilm" class="desc"><?php echo $redact['descriptionFilm'] ?></textarea></th>
        </tr>
        <tr>
            <th>Кількість переглядів:</th>
            <th><input type="text" class="text" name="countViews" value="<?php echo $redact['countViews'] ?>"></th>
        </tr>
        <tr>
            <th>Кількість лайків:</th>
            <th><input type="text" class="text" name="countLikes" value="<?php echo $redact['countLikes'] ?>"></th>
        </tr>
        <tr>
            <th>Постер:<span class="red"> *</span></th>
            <th><input type="file" name="imageFilm" accept="image/jpeg"></th>
        </tr>
        <tr>
            <th>Відео:<span class="red"> *</span></th>
            <th><input type="file" name="videoFilm" accept="video/mp4"></th>
        </tr>
        <tr>
            <th>Сторінка: <span class="red">*</span></th>
            <th><input type="file" name="pageFilm" accept=".php"></th>
        </tr>
    </table>
        <em><strong><span class="red">*</span> - обов'язкові</strong></em>
    <input type="submit" name="save" value="Зберегти" class="btn-blue">
    </form>
</div>
</body>

