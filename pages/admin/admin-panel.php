<?php
include 'admin-params.php';
?>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>Дивитися фільми онлайн</title>
    <!--Іконки різних розширень-->
    <link rel="apple-touch-icon" sizes="57x57" href="../../icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../../icon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../../icon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../../icon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../../icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../../icon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../../icon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../../icon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../../icon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="../../icon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../../icon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../icon/favicon-16x16.png">
    <link rel="manifest" href="icon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="icon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!--Font Awesome-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" rel="stylesheet">
    <!--Стилі-->
    <link href="../../css/admin-panel/style.css" rel="stylesheet">
    <!--Шрифт-->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="flex-header">
        <div class="h1">Панель адміністратора</div>
        <a class="exit" style="text-decoration: none" href="../../index.php"><i class="fas fa-door-open"></i> Вихід</a>
    </div>
    <div class="head">Статистика за 6 місяців</div>
    <div class="flexbox">
        <div class="col">
            <div class="flexboxSpaceBetween">
                <div class="row-item">Коментарі</div>
                <div class="icon"><i class="fas fa-arrow-right"></i></div>
            </div>
            <canvas id="myChart"></canvas>
        </div>
        <div class="col">
            <div class="flexboxSpaceBetween">
                <div class="row-item">Статті</div>
                <div class="icon"><i class="fas fa-arrow-right"></i></div>
            </div>
            <canvas id="myChart1"></canvas>
        </div>
        <div class="col">
            <div class="flexboxSpaceBetween">
                <div class="row-item">Користувачі</div>
                <div class="icon"><i class="fas fa-arrow-right"></i></div>
            </div>
            <canvas id="myChart2"></canvas>
        </div>
    </div>
    <div class="result-table" style="border: 1px solid black">
        <table>
            <caption>Таблиця "Фільми"</caption>
            <form action="add.php" method="post">
                <button class="btn-redact new"><i class="fas fa-plus"></i> Новий запис</button>
            </form>
            <tr class="first-row">
                <th>idFilm</th>
                <th>nameFilm</th>
                <th>yearFilm</th>
                <th>country</th>
                <th>genre</th>
                <th>timeFilm</th>
                <th>descriptionFilm</th>
                <th>countViews</th>
                <th>countLikes</th>
                <th>image</th>
                <th>page</th>
                <th>video</th>
                <th>datePublication</th>
                <th></th>
                <th></th>
            </tr>
            <?php for ($i=0;$i<$count;$i++) {
                echo '<tr>';
                $res = mysqli_fetch_assoc($films);
                echo '<th>'.$res['idFilm'].'</th>';
                echo '<th>'.$res['nameFilm'].'</th>';
                echo '<th>'.$res['yearFilm'].'</th>';
                echo '<th>'.$res['country'].'</th>';
                echo '<th>'.$res['genre'].'</th>';
                echo '<th>'.$res['timeFilm'].'</th>';
                echo '<th>'.mb_substr($res['descriptionFilm'],0, 50, 'UTF-8').'...</th>';
                echo '<th>'.$res['countViews'].'</th>';
                echo '<th>'.$res['countLikes'].'</th>';
                echo '<th>'.$res['image'].'</th>';
                echo '<th>'.$res['page'].'</th>';
                echo '<th>'.$res['video'].'</th>';
                echo '<th>'.$res['datePublication'].'</th>';
                echo '<th><a href="redact.php?redact='.$res['idFilm'].'" class="btn-redact">Редагувати</th>';
                echo '<th><a href="../../delete.php?delete='.$res['idFilm'].'" class="btn-redact">Видалити</th>';
                echo '</tr>'; }
            ?>
        </table>
    </div>

</div>


<script>
    var f1 = '<?php echo implode($f1) ?>';
    var f2 = '<?php echo implode($f2) ?>';
    var f3 = '<?php echo implode($f3) ?>';
    var f4 = '<?php echo implode($f4) ?>';
    var f5 = '<?php echo implode($f5) ?>';
    var f6 = '<?php echo implode($f6) ?>';

    var c1 = '<?php echo implode($c1) ?>';
    var c2 = '<?php echo implode($c2) ?>';
    var c3 = '<?php echo implode($c3) ?>';
    var c4 = '<?php echo implode($c4) ?>';
    var c5 = '<?php echo implode($c5) ?>';
    var c6 = '<?php echo implode($c6) ?>';

    var u1 = '<?php echo implode($u1) ?>';
    var u2 = '<?php echo implode($u2) ?>';
    var u3 = '<?php echo implode($u3) ?>';
    var u4 = '<?php echo implode($u4) ?>';
    var u5 = '<?php echo implode($u5) ?>';
    var u6 = '<?php echo implode($u6) ?>';
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
<script src="../../js/admin-panel.js"></script>
</body>
</html>